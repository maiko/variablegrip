import argparse
# This script reads a file and cuts off any lines until a line is found that contains the given criterion
# So if the the criterion is 'b' and the first occurence of 'b' is in line 5, lines 1-4 will be cut from the file and only ' # line 5 and further is kept
def main():    # you can name this whatever you want, it doesn't need to be main()
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", help = "Input file to extract from")
    parser.add_argument("-c", "--criterion", help = "the criterion to select lines with")
    parser.add_argument("-u16", "--utf16le", action = 'store_true', help = "whether to read the input file with utf-16-le encoding")
    parser.add_argument("-o", "--output", help = "output file that will be created or overwritten")
    args = parser.parse_args()
    cut(args.file, args.criterion, args.utf16le, args.output)

def cut(filename, criterion, u16, outputFilename):
    encoding = 'utf-16-le' if u16 else 'utf-8'
    with open(filename, encoding=encoding) as f:
        lines = f.readlines()
    output = ""
    firstOccurence = 0
    for i in range(len(lines)):
        if criterion in lines[i]:
            firstOccurence = i
            break
    for l in lines[firstOccurence:]:
        output += l
    with open(outputFilename, 'w') as f_out:
        f_out.write(output)
    
if __name__ == '__main__':
    main()