import argparse
# The idea of this script is to move values found in one type of message (line) into all the following lines
# until another occurence of that message is found, in which case the values of that newer message will be copied to
# the following lines. If line 1 ('a, foo, bar') exclusively contains the criterion 'a' then it will be selectes and
# the given values will be parsed, for example the argument at pos 1 which is 'foo'. Now all the following lines will
# get 'foo' appended until another line containing 'a' is found, e.g. line 12 ('a, far, boo'). Now each line will get
# 'far' appended.

def main():    # you can name this whatever you want, it doesn't need to be main()
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", help = "Input file to extract from")
    parser.add_argument("-c", "--criterion", help = "the criterion to select lines with")
    parser.add_argument("-l", "--labels", help = "(position,label) tuples, separated with semicolons, labels currently have no use but are required)")
    parser.add_argument("-p", "--pre", action = 'store_true', help = "whether to put the values at the start of the row")
    parser.add_argument("-u16", "--utf16le", action = 'store_true', help = "whether to read the input file with utf-16-le encoding")
    parser.add_argument("-o", "--output", help = "output file that will be created or overwritten")
    args = parser.parse_args()
    moveValues(args.file, args.criterion, args.labels, args.pre, args.utf16le, args.output)

def moveValues(filename, criterion, labels, putInFront, u16, outputFilename):
    tuples = labels.split(';')
    newTuples = []
    for t in tuples:
        newTuples.append(t.split(','))
    tuples = newTuples

    encoding = 'utf-16-le' if u16 else 'utf-8'
    with open(filename, encoding=encoding) as f:
        lines = f.readlines()

    output = ""
    currentValues = []
    currentLabels = []
    for i in range(len(lines)):
        if criterion in lines[i]:
            currentValues = []
            currentLabels = []
            for j in range(len(tuples)):
                currentValues.append(lines[i].split(';')[int(tuples[j][0])])
                currentLabels.append(tuples[j][1]) #currently unused
        else:
            stringList = lines[i].split(';')[:-1]
            lineWithoutNewLine = ';'.join(stringList) + ';'
            currentValuesToMove = ''
            for v in currentValues:
                currentValuesToMove += v + ';'
            
            if putInFront:
                lines[i] = currentValuesToMove + lineWithoutNewLine + '\n'
            else:
                lines[i] = lineWithoutNewLine + currentValuesToMove + '\n'
        output += lines[i]

    with open(outputFilename, 'w') as f_out:
        f_out.write(output)
               
    # 497,7382; Task changed; B ;Binary;        B = pos 2, label "Task"; Binary = pos 3, label = "Method"..... -> 2,Task;3,Method        
    
if __name__ == '__main__':
    main()