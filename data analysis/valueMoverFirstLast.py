import argparse

def main():    # you can name this whatever you want, it doesn't need to be main()
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", help = "Input file to extract from")
    parser.add_argument("-kfc", "--keepFirstCriterion", help = "the criterion to select lines with whose first occurence will be entered in the paste line")
    parser.add_argument("-kfp", "--keepFirstPos", help = "the position of the word in the line to copy from for the first occurence line")
    parser.add_argument("-klc", "--keepLastCriterion", help = "the criterion to select lines with whose last occurence will be entered in the paste line")
    parser.add_argument("-klp", "--keepLastPos", help = "the position of the word in the line to copy from for the last occurence line")
    parser.add_argument("-c", "--pasteCriterion", help = "the criterion to select the line with, where the collected values will be pasted")
    # parser.add_argument("-p", "--pre", action = 'store_true', help = "whether to put the values at the start of the row")
    parser.add_argument("-u16", "--utf16le", action = 'store_true', help = "whether to read the input file with utf-16-le encoding")
    parser.add_argument("-o", "--output", help = "output file that will be created or overwritten")
    args = parser.parse_args()
    moveValues(args.file, args.keepFirstCriterion, args.keepFirstPos, args.keepLastCriterion, args.keepLastPos, args.pasteCriterion, args.utf16le, args.output)

def moveValues(filename, kfc, kfp, klc, klp, pasteCriterion, u16, outputFilename):

    encoding = 'utf-16-le' if u16 else 'utf-8'
    with open(filename, encoding=encoding) as f:
        lines = f.readlines()

    output = ""

    foundFirstOccurence = False
    firstOccurenceValue = ''
    lastOccurenceValue = ''
    for i in range(len(lines)):
        if kfc in lines[i] and foundFirstOccurence is False:
            foundFirstOccurence = True
            firstOccurenceValue = lines[i].split(';')[int(kfp)]
        elif klc in lines[i]:
            lastOccurenceValue = lines[i].split(';')[int(klp)]
        elif pasteCriterion in lines[i]:
            stringList = lines[i].split(';')[:-1]
            lineWithoutNewLine = ';'.join(stringList) + ';'
            lines[i] = lineWithoutNewLine + ' ' + firstOccurenceValue + '; ' + lastOccurenceValue + ';' + '\n'

            foundFirstOccurence = False
            firstOccurenceValue = ''
            lastOccurenceValue = ''

        output += lines[i]

    with open(outputFilename, 'w') as f_out:
        f_out.write(output)
               
    # 497,7382; Task changed; B ;Binary;        B = pos 2, label "Task"; Binary = pos 3, label = "Method"..... -> 2,Task;3,Method        
    
if __name__ == '__main__':
    main()