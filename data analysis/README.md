# README
This is a guide to the usable files in this directory and how they are processed.

## Usable Files
All output files have a jasp file with a corresponding name except logs.csv
Generally all questionnaires (except demographics) also have a paired version (paired on participant), both as .csv and as a JASP project, so the paired wilcoxon test can be made.

- demo_clean.csv
- demographics_clean.csv (created by Michael) Note: Doesn't contain the corrected data for two participants who mistakenly entered that they feel very experienced in VR while having never used VR before. Corrected in demographics.csv and therefore in demo_clean.csv after one more pipeline run
- cstm_clean.csv
- cstm_clean_paired.csv
- tlx_clean.csv
- tlx_clean_paired.csv.csv
- pq_clean.csv
- pq_clean_paired.csv.csv
- all_questionnaires_combined.csv
- logs.csv
- logs_paired.csv

- demo_clean.jasp
- demographics_clean.jasp (created by Michael) See note above
- cstm_clean.jasp
- cstm_clean_paired.csv.jasp
- tlx_clean.jasp
- tlx_clean_paired.csv.jasp
- pq_clean.jasp
- pq_clean_paired.csv.jasp
- all_questionnaires_combined.jasp
- logs_paired.jasp

- Glove Notebook.ipynb
- cleaner.py

- plots.R

## Pipelines

For all files processed by jupyterlab the Glove Notebook.ipynb was used. It contains commented code that explains what is happening and why.

The logs where preprocessed in a python script called cleaner.py. This accesses multiple other scripts. Unfortunately those scripts aren't constructed that well and hard to understand. The main objective was to extract uniform data that contains all relevant datapoints for further cleaning, recalculation and analysis.

running cleaner.py will read all the files called like participant-p[1-21]-2022-11-[...]_log_file.csv and create a lot of files that aren't needed (namely 189). The actual result is contained in all_participants_rearranged.csv, all other new files can be safely discarded, see git status. (Yes, this is not done well ;) )

### Demographics
demographics.csv -> jupyterlab -> demo_clean.csv

### Custom Questions
questionnaireID_custom_ALL_answers.csv'  -> jupyterlab -> cstm_clean.csv, cstm_clean_paired.csv

### TLX
questionnaireID_NASATLX_ALL_answers.csv' -> jupyterlab -> tlx_clean.csv, tlx_clean_paired.csv.csv

### PQ
questionnaireID_WM_PQ_ALL_answers.csv -> jupyterlab -> pq_clean.csv, pq_clean_paired.csv.csv

### Logs (Time & Precision Measurements)
participant-p[1-21]-2022-11-[...]_log_file.csv -> cleaner.py -> all_participants_rearranged.csv -> jupyterlab -> logs.csv, logs_paired.csv

## Plots

the plots.R script was used to create the plots. The script contains further explanations on its usage.

t uses the following data files which were created in the jupyterlab pipeline.

- logs_paired.csv
- all_questionnaires_combined_paired.csv
-> creates all files in the 'rplots' directory
