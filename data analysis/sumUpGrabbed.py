import argparse

# reconstructs the grabbed time by summing up the individual grab times from the release Events of the corresponding object 
def main():    # you can name this whatever you want, it doesn't need to be main()
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", help = "Input file to extract from")
    parser.add_argument("-u16", "--utf16le", action = 'store_true', help = "whether to read the input file with utf-16-le encoding")
    parser.add_argument("-o", "--output", help = "output file that will be created or overwritten")
    args = parser.parse_args()
    sumUp(args.file, args.utf16le, args.output)

def sumUp(filename, u16, outputFilename):
    encoding = 'utf-16-le' if u16 else 'utf-8'
    with open(filename, encoding=encoding) as f:
        lines = f.readlines()

    output = ""
    grabbed = 0.0
    released = 0.0
    released_count = 0
    grabbed_time = 0
    grabbed_item_name = None
    for i in range(len(lines)):
        if "Grabbed" in lines[i]:
            grabbed_item_name = lines[i].split(';')[2]
        if "Released" in lines[i]:
            split = lines[i].split(';')
            released_item_name = split[2]
            if released_item_name == grabbed_item_name: #need to check this because there have been cases where there are release events of an item already placed (it got re-grabbed)         
                released_count+= 1
                this_grab_duration = split[-2] #the released event contains the grabbed duration
                grabbed_time+= float(this_grab_duration[1:].replace(',', '.')) #remove whitespace, change comma to dot then parse as float
        if "Placement" in lines[i]:
            lines[i] = lines[i][:-1] + " " + str(grabbed_time).replace('.', ',') + "; " + str(released_count) + ";\n"
            released_count = 0
            grabbed_time = 0.0
            grabbed_item_name = None
        output += lines[i]
    
    with open(outputFilename, 'w') as f_out:
        f_out.write(output)    
    
if __name__ == '__main__':
    main()