import argparse
import os
import cutter
import valueMover
import valueMoverFirstLast
import extractor
import sumUpGrabbed
import re

finalFilePrefix = 'final-participant-p'

def main():    # you can name this whatever you want, it doesn't need to be main()
    # parser = argparse.ArgumentParser()
    # parser.add_argument("-d", "--directory", help = "Directory to read input files from")
    # args = parser.parse_args()
    # clean(args.directory) commented out because the program currently only accepts its own directory as input
    
    clean()
    concatenate()

#gets the right files and sorts them as intended
def getFiles(boolean_filter, enableSort):
    files = os.listdir(os.getcwd())
    files = [f for f in files if boolean_filter(f)]
    if enableSort:
        files.sort(key = lambda f: int(re.search('(final-)?(?<=participant-p)([0-9]+)', f)[0]))
    return files

def clean():
    files = getFiles(lambda f: ('participant-p' in f) and f.endswith('.csv'), False)
    for f in files:
        if f.endswith('.csv'):
            convert(f)
    
def convert(file):
    cutterOutputFilename = addSuffix(file, '_cut')
    cutter.cut(file, 'Task', False, cutterOutputFilename)  #remove all measures until first task was started, which is when the word 'Task' has its first occurence
    timestampsOutputFileName = addSuffix(cutterOutputFilename, '_added_timestamps')
    valueMoverFirstLast.moveValues(cutterOutputFilename, 'Grabbed', '0', 'Released', '0', 'Placement', False, timestampsOutputFileName) #grab the first grab and last release timestamp and put them into the final placement row
    grabbedRecalculatedOutputFilename = addSuffix(timestampsOutputFileName, '_grabbed_recalculated')
    sumUpGrabbed.sumUp(timestampsOutputFileName, False, grabbedRecalculatedOutputFilename) #recalculate the grabbed times
    extractorOutputFilename = addSuffix(grabbedRecalculatedOutputFilename, '_extracted')
    extractor.extract(grabbedRecalculatedOutputFilename, 'Placement,Task changed', False, extractorOutputFilename) #extract all rows with placement or task change indicator
    valueMoverOutputFilename = addSuffix(extractorOutputFilename, '_values_moved')
    valueMover.moveValues(extractorOutputFilename, 'Task', '2,Task;3,Method', True, False, valueMoverOutputFilename) # move task type out of task change rows into placement rows
    #finalFileName = 'final_' + valueMoverOutputFilename[:len('_cut_extracted_values_moved.csv')] + '.csv'
    cleanFileNameOutput = addSuffix(valueMoverOutputFilename, '_extracted')
    extractor.extract(valueMoverOutputFilename, 'Placement', False, cleanFileNameOutput) # discard task change rows

    with open(cleanFileNameOutput) as f: #add participant number as first row so i can reuse valueMover
        lines = f.readlines()
    participantID = re.search('[0-9]+', re.search('(participant-p)[0-9]+', cleanFileNameOutput)[0])[0]
    output = 'participant;' + participantID + ';' + '\n'
    for l in lines:
        output += l

    addedIDOutputName = addSuffix(cleanFileNameOutput, '_added_id')
    with open(addedIDOutputName, 'w') as f_out:
        f_out.write(output)

    idMovedOutputName = 'participant-' + participantID + '_id_moved.csv'
    valueMover.moveValues(addedIDOutputName, 'participant', '1,id', True, False, idMovedOutputName)

    extractor.extract(idMovedOutputName, 'Placement', False, finalFilePrefix + participantID + '.csv')

def concatenate():
    files = getFiles(lambda f: f.endswith('.csv') and f.startswith(finalFilePrefix) , True)
    master = "ID;Task;Condition;Timestamp;event;Item_name;Distance_Offset;Offset_Angle;Total_Duration_until_placement;Amount_of_Grabs;Duration_grabbed;Duration_Fixed;Duration_loose;Duration_ungrabbed_in_between;Duration_total_ungrabbed;Avg_Heuristic_Score;First_Grab_Timestamp;Last_Release_Timestamp;Recalculated_Duration_grabbed;Recalculated_Amount_of_Grabs"
    master += '\n'
    for f in files:
        with open(f) as c:
            lines = c.readlines()
        for l in lines:
            master += l
    with open('all_participants_rearranged.csv', 'w') as f_out:
        f_out.write(master)

# in "p1results.csv", "_altered" out: p1results_altered.csv
def addSuffix(filename, suffix): 
    fileEnding = '.csv'
    return filename[:-len(fileEnding)] + suffix + fileEnding

if __name__ == '__main__':
    main()