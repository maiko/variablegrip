#!/bin/sh
files_to_move=(
density_bf_task_B_items/Milk_density_bf_task_B_Distance_Offset.png
box_cstm_bf.png
box_logs_bf.png
box_pq_bf.png
box_tlx_bf.png
density_bf_task_A_Duration_grabbed.png
density_bf_task_A_Offset_Angle.png
density_bf_task_B_Offset_Angle.png
	)
for f in ${files_to_move[@]};
do
	cp $f ~/Documents/Universität/Bachelor-Arbeit/Thesis/images/plots/
done