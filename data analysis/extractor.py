import argparse
# This script reads a file and a comma-split list of strings as criterions. Every line
# in the file that contains at least one of the criterions will be kept, all other lines will be cut out.

def main():    # you can name this whatever you want, it doesn't need to be main()
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", help = "Input file to extract from")
    parser.add_argument("-c", "--criterions", help = "the criterions to select lines with, split with commas for multiple criterions")
    parser.add_argument("-u16", "--utf16le", action = 'store_true', help = "whether to read the input file with utf-16-le encoding")
    parser.add_argument("-o", "--output", help = "output file that will be created or overwritten")
    args = parser.parse_args()
    extract(args.file, args.criterions, args.utf16le, args.output)

def extract(filename, criterions, u16, outputFilename):
    criterions = criterions.split(',')
    
    encoding = 'utf-16-le' if u16 else 'utf-8'
    with open(filename, encoding=encoding) as f:
        lines = f.readlines()

    output = ""
    saveString = False
    for l in lines:
        for c in criterions:
            if c in l:
                saveString = True
        if saveString:
            output += l
        saveString = False
    with open(outputFilename, 'w') as f_out:
        f_out.write(output)
    
if __name__ == '__main__':
    main()