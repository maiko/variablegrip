# Variable Grip with Force-Feedback Gloves
![](teaser.jpg)

## What is this about
This repository contains the all of the resources I created for and used in my bachelor's thesis.
The goal was to create a VR interaction prototype for a force-feedback glove which allows the user to rotate grabbed items in the hand. By varying the applied grip strength, the user can switch between a fixed grip and a loose grip. I conducted a within-subject study (n=21) to evaluate the prototype. You can find an abstract below.


## Content of the repository
This repository is mainly the unity project which contains the code for the prototype and the unity scenes + assets used to conduct the study.


## Data Analysis
The 'data_analysis' directory contains the gathered data and the scripts that were used to clean, rearrange and conduct the data analysis. Additionally the script used to generate the plots and the plots themselves are included. An additional README.md is included in the data_analysis directory wich explains what the files are for.

## Abstract
This thesis explores the use of variable grip strength in virtual reality (VR) for object manipulation. For this purpose, an interaction technique with a *SenseGlove* force feedback glove was developed, in which the virtual grip strength is derived from the force applied in real life. As a result, gripped objects can be rotated between the fingers. Conventionally, objects can only be gripped firmly. A *within-subject* user study (N=21) was conducted comparing the new interaction technique with the conventional method. Data on precision of placed objects and temporal effort was recorded, and questionnaires on *presence* in VR (*the feeling of being there*), mental load, and self-composed questionnaires were used. The conventional method achieved slightly better results in the areas of precision and time (on average 1 mm more precise per item, 1.8 s faster), yet the two methods did not always differ significantly in other respects. The results show that variable grip strength using purely kinesthetic feedback still poses some challenges and the prototype should be reevaluated. For this purpose, a discussion of promising approaches for further research is presented.