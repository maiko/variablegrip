using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HTC.UnityPlugin.Vive;

public enum GoalZoneMode {
    KeepObjectInPlace,
    ReplaceObjects,
    DisableObjects,
    ContinueWithNext
}

public class GoalZone : MonoBehaviour
{

    // Prefab that will be respawned. Must have childeren with the Grabable and Rendered roles.
    public GameObject acceptedObjectParentPrefab;

    // The tag of the accepted prefab, make sure it is entered and unique to this kind of object.
    private string acceptedObjectTag;

    public float minimalVelocity = 0.1f;
    public float minimalAngularVelocity = 0.1f;

    public int attempts = 15;

    public int currentAttempt = 1;

    public bool replaceObjects = true;

    public GoalZoneMode goalZoneMode = GoalZoneMode.ReplaceObjects;

    public bool objectCorrectlyPlaced {get; private set;} = false;

    public GameObject experimentManager;

    private LoggingManager loggingManager;

    public List<GameObject> collidingObjects;

    private ControlLogic controlLogic;

    // allowed positional difference per axis
    public float allowedPosOffset = 0.10f;

    // allowed rotational difference per axis in euler degrees
    public float allowedRotOffset = 10f;

    private Bounds thisBounds;

    // Start is called before the first frame update
    void Start()
    {
        collidingObjects = new List<GameObject>();

        foreach (Transform child in acceptedObjectParentPrefab.transform)
        {
            if (child.name.Contains("Rendered")) {
                acceptedObjectTag = child.tag; // TODO FIX will only work for single kind of object runs
            }
        }

        loggingManager = experimentManager.GetComponent<LoggingManager>();
        controlLogic = experimentManager.GetComponent<ControlLogic>();
        thisBounds = this.GetComponent<Renderer>().bounds;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Backspace)) {
            Instantiate(acceptedObjectParentPrefab);
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (objectCorrectlyPlaced || isNotASphereCollider(other))
        {
            return;
        }

        GameObject otherGameObject = other.gameObject;
        if (otherGameObject.CompareTag(acceptedObjectTag))
        { //is the right kind of object
            Transform parentTransform = otherGameObject.transform.parent;
            VariableGrip vari = parentTransform.GetComponentInChildren<VariableGrip>();
            BasicGrabbable basicGrabbable = parentTransform.GetComponentInChildren<BasicGrabbable>(); //this is the grabbable of the vive input utility (VIU) which exists in the objects for the additional "controller" study
            bool basicGrabbableIsGrabbed = (basicGrabbable != null) ? basicGrabbable.isGrabbed : false;
            if (vari.isGrabbed == false && basicGrabbableIsGrabbed == false)
            { // is not grabbed anymore (user decided to place)

                Rigidbody otherRB = otherGameObject.GetComponent<Rigidbody>();
                if (otherRB.velocity.magnitude < minimalVelocity && otherRB.angularVelocity.magnitude < minimalAngularVelocity)
                { // is not drifting or falling anymore
                  //check if object is within positional tolerance
                    Vector3 posOffset = getPosOffset(otherGameObject);  //takes the actual center so the pivot point can be wherever
                    if (posOffset.x <= allowedPosOffset && posOffset.y <= allowedPosOffset && posOffset.z <= allowedPosOffset)
                    {

                        //check if object is within rotational tolerance range in each axis.
                        float angularOffset = getAngularOffset(other.transform);
                        if (angularOffset <= allowedRotOffset)
                        {

                            objectCorrectlyPlaced = true;
                            StartCoroutine(objectPlaced(otherGameObject));
                        }

                    }

                }

            }

        }

    }

    private Vector3 getPosOffset(GameObject other) {
        return thisBounds.center - other.GetComponent<Renderer>().bounds.center;
    }

    private float getAngularOffset(Transform other) {
        return Quaternion.Angle(this.transform.rotation, other.transform.rotation);
    }

    void OnTriggerEnter(Collider other) {
        if(isNotASphereCollider(other)) {
            return;
        }
        GameObject otherGO = other.gameObject;
        if(otherGO.CompareTag(acceptedObjectTag)) {
            collidingObjects.Add(otherGO);
        }
    }

    void OnTriggerExit(Collider other) {
        if(isNotASphereCollider(other)) {
            return;
        }
        collidingObjects.Remove(other.gameObject);
    }

    private bool isNotASphereCollider(Collider c) {
        if(c is SphereCollider) return false; else return true;
    }

    IEnumerator objectPlaced(GameObject placedObject) {
        yield return new WaitForSeconds(0.5f);
        GameObject parentObject = placedObject.transform.parent.gameObject;
        currentAttempt++;
        // gets the offsets once again, in ase they have further stabilized after 0.5 seconds
        loggingManager.writePlacementToLog(placedObject, this.gameObject, getAngularOffset(placedObject.transform), getPosOffset(placedObject));

        if(goalZoneMode == GoalZoneMode.ReplaceObjects) {
            HotkeyPosition hkp = parentObject.GetComponent<HotkeyPosition>();
            GameObject newObject  = Instantiate(acceptedObjectParentPrefab, hkp.startPos, hkp.startRot);
            newObject.name = newObject.name + " " + currentAttempt.ToString();
        }

        if(goalZoneMode == GoalZoneMode.ContinueWithNext) {
            controlLogic.enableNextObject();
        }

        if(goalZoneMode != GoalZoneMode.KeepObjectInPlace) {
            parentObject.SetActive(false);
            objectCorrectlyPlaced = false;
        }

    }

}
