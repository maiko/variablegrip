using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandUIController : MonoBehaviour
{
    public Transform cameraTrans;
    public VariableGrip currentlyHeldObjectVG;

    private ForceInputBar floorBar;
    private ForceInputBar midBar;
    private ForceInputBar fixedBar;

    private List<ForceInputBar> barList;

    public bool showGraspQualityDebugText = false;

    public float smoothingFactor = 0.9f;

    private float lastValue = 0f;

    // Start is called before the first frame update
    void Start()
    {
        floorBar = transform.Find("ForceInputBarFloor").GetComponent<ForceInputBar>();
        midBar = transform.Find("ForceInputBarMid").GetComponent<ForceInputBar>();
        fixedBar = transform.Find("ForceInputBarFixed").GetComponent<ForceInputBar>();
        barList = new List<ForceInputBar>(){floorBar, midBar, fixedBar};
        if(barList.Count != 3) {
            throw new Exception("The three ForceInputBars haven't been found!");
        }

        floorBar.startValue = 0;
        fixedBar.endValue = 100;

        currentlyHeldObjectVG = new VariableGrip(); //create dummy values for the tim euntil the first real object is grabbed, dirty trick that will produce a warning
        currentlyHeldObjectVG.lowForceThreshold = 0;
        currentlyHeldObjectVG.fixedJointThreshold = 0;

        this.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(cameraTrans);
        transform.rotation *= Quaternion.Euler(0, 180, 0);


        if(currentlyHeldObjectVG.gripVariant == GripVariant.Binary) {
            floorBar.endValue = currentlyHeldObjectVG.fixedJointThreshold;
            midBar.startValue = 0;
            midBar.endValue = 0;
        } else { // the usual case (variable)
            floorBar.endValue = currentlyHeldObjectVG.lowForceThreshold;
            midBar.startValue = currentlyHeldObjectVG.lowForceThreshold;
            midBar.endValue = currentlyHeldObjectVG.fixedJointThreshold;
        }
        fixedBar.startValue = currentlyHeldObjectVG.fixedJointThreshold;

        fillAll(currentlyHeldObjectVG.forceLevel);
    }

    public void OnDisable(){
        if(barList != null) {
            Boolean started = true;
            foreach(ForceInputBar f in barList) { //find out if all objects have been actually started
                if(f.started == false) {
                    started = false;
                }
            }
            if(started) {
                fillAll(0f);
            }
        }
    }

    //fills all inputBars, if configured right, the instances will know how to take care of the value correctly
    void fillAll(float forceLevel) {
        if(forceLevel == 0f) {
            lastValue = 0f;
        }
        forceLevel = Mathf.Lerp(lastValue, forceLevel, Time.deltaTime * smoothingFactor);
        lastValue = forceLevel;
        barList.ForEach(f => f.fill(forceLevel));
    }

}
