using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariableGripSetup : MonoBehaviour
{

    public Transform indexTip;

    public Transform thumbTip;

    // the feedback layer of one hand. Currently the new interaction method is restricted to one hand.
    public GameObject feedbackLayer;

    public GameObject FingerMiddle;

    public GameObject handUI;

    public HandUIController handUIController;

    public bool started = false;

    public List<GameObject> penetrableObjects; //all objects that should be penetrable by hand when a grabable is held (grabable still inpenetrable) eg. shelves //

    // Start is called before the first frame update
    void Start()
    {
        handUIController = handUI.GetComponent<HandUIController>();
        //TODO throw error if controller is not there
        //TODO should find glove related game objects on its own depending on choice of left or right hand
        //TODO should check out whether the components are attached
        started = true;
    }

    public void makePenetrable() {
        penetrableObjects.ForEach(x => x.layer = 7); //seven is the penetrableSurface layer
    }

    public void makeInpenetrable() {
        penetrableObjects.ForEach(x => x.layer = 0); //zero is the default layer
    }


}
