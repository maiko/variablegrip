using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToGlobalFingerMiddle : MonoBehaviour
{        
    private Transform indexTip;

    private Transform thumbTip;

    private VariableGripSetup variableGripSetup;

    // Start is called before the first frame update
    void Start()
    {
        GameObject setupObject = GameObject.Find("Variable Grip Setup");
        variableGripSetup = setupObject.GetComponent<VariableGripSetup>();
        indexTip = variableGripSetup.indexTip;
        thumbTip = variableGripSetup.thumbTip;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(indexTip.position, thumbTip.position, 0.5f);
    }
}
