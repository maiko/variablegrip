using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Moves gameobject back to its initial position and rotation. Children objects will be reset to zero position and rotation. 
// This assumes that children are all on local position zero initially which is rather uncommon.
// Author: Maiko Huebinger
public class HotkeyPosition : MonoBehaviour
{
    
    public KeyCode keyCode;
    
    public Vector3 startPos {get; private set;}

    public Quaternion startRot {get; private set;}
    public bool initialized {get; private set;} = false;


    // Start is called before the first frame update
    void Start()
    {
        startPos = this.transform.position;
        startRot = this.transform.rotation;
        initialized = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(keyCode)) {
            resetPosition();
        }
    }

    public void resetPosition() {
        this.transform.position = startPos;
        this.transform.rotation = startRot;
        foreach(Transform c in transform) {
            c.localPosition = Vector3.zero;
            c.localRotation = Quaternion.Euler(Vector3.zero);
            c.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }
}
