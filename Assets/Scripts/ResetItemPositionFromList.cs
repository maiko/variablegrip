using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetItemPositionFromList : MonoBehaviour
{

    public List<GameObject> items = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void resetItems() {
        foreach(GameObject i in items) {
            HotkeyPosition hkp = i.GetComponent<HotkeyPosition>();
            hkp.resetPosition();
        }
    }
}
