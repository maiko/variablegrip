using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
using VRQuestionnaireToolkit;
using System.Threading.Tasks;

public class ControlLogic : MonoBehaviour
{

    public string participantID; //enter here, the ID will be propagated to the questionnaire toolkit

    public bool taskAThenB = true;

    public bool binaryThenFixed = true;

    private bool currentlyInTaskA;

    private bool currentlyUsingBinaryGrip;

    public bool activatePlayGround = false;

    public bool activateDemographics = true;

    private LoggingManager loggingManager;

    public float binaryGripThreshold = 50f;

    //parent objects containing all relevant objects for the phase
    public GameObject playGroundObject;

    public GameObject taskAObject;

    public GameObject taskBObject;

    //Lists containing all 
    public List<GameObject> playGroundObjects;

    public List<GameObject> taskAObjects;

    public List<GameObject> taskBObjects;

    private int taskAcurrentActiveIndex;
    
    private int taskBcurrentActiveIndex;

    public VariableGripSetup variableGripSetup;


    private GameObject vRQuestionnaireToolkit;

    private GenerateQuestionnaire generateQuestionnaire;

    private ExportToCSV exportToCsv;

    List<GameObject> questionnaires;

    public int currentQuestionnaire = 0; // -1 means no questionnaire has started yet

    StudySetup studySetup;

    public GameObject bookGoal;
    public GameObject milkGoal;
    public GameObject soupGoalB;
    public GameObject soupGoalA;

    public GameObject firstHint;

    public GameObject secondHint;




    // Start is called before the first frame update
    async Task Start()
    {
        vRQuestionnaireToolkit = GameObject.FindGameObjectWithTag("VRQuestionnaireToolkit");
        studySetup = vRQuestionnaireToolkit.GetComponent<StudySetup>();
        generateQuestionnaire = vRQuestionnaireToolkit.GetComponent<GenerateQuestionnaire>();
        exportToCsv = vRQuestionnaireToolkit.GetComponentInChildren<ExportToCSV>();
        exportToCsv.QuestionnaireFinishedEvent.AddListener(finishedAQuestionnaire);
        questionnaires = vRQuestionnaireToolkit.GetComponentInChildren<GenerateQuestionnaire>().Questionnaires;
        studySetup.ParticipantId = participantID;
        taskAObjects = flattenGameObjectList(taskAObjects);
        taskBObjects = flattenGameObjectList(taskBObjects);
        taskBObjects = taskBObjects.OrderBy(x => Random.value).ToList(); // shuffle the objects once for this participant
        foreach(GameObject o in taskAObjects.Concat(taskBObjects).Concat(playGroundObjects)) { //wait for all objects to have their initialPosition saved
            await initializeHotKeyPosition(o);
        }
        loggingManager = GetComponent<LoggingManager>();
        loggingManager.setParticipantID(participantID);
        questionnaires[0].SetActive(false); // disable the first questonnaire, but will be automaticly reenabled if needed
        StartCoroutine(waitUntilVGSstarted()); // necessary because all variableGrips should finish setup before they get disabled eventually  
    }

    IEnumerator waitUntilVGSstarted() {
        yield return new WaitUntil(() => variableGripSetup.started); //only needed when called from start()???
        configureTaskObjectActivity();
    }

    // events such as tutorials for the tsk could be injected here
    public void finishPlayGround() {
        if(activatePlayGround){
            activatePlayGround = false;
            configureTaskObjectActivity();
        }
    } 

    private void configureTaskObjectActivity() {
        if(activateDemographics) { // regular start with first questionnaire, no objects should be seen
            questionnaires[0].SetActive(true); // disable the first questonnaire
            playGroundObject.SetActive(false);
            taskAObject.SetActive(false);
            taskBObject.SetActive(false);
        } else if(activatePlayGround) {
            playGroundObject.SetActive(true);
            taskAObject.SetActive(false);
            taskBObject.SetActive(false);
        } else {
            startTask(binaryThenFixed, taskAThenB);
        }
    }

    async Task initializeHotKeyPosition(GameObject o) {
        bool wasActive = o.activeSelf;
        o.SetActive(true);
        HotkeyPosition hkp = o.GetComponent<HotkeyPosition>();
        while(!hkp.initialized) {
            await Task.Yield();
        }
        o.SetActive(wasActive);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void startTask(bool useBinaryGrip, bool startTaskA) {
        currentlyInTaskA = startTaskA;
        currentlyUsingBinaryGrip = useBinaryGrip;
        var task = loggingManager.writeCurrentTaskConfigurationToLog(currentlyInTaskA, currentlyUsingBinaryGrip);
        List<GameObject> taskObjects = startTaskA? taskAObjects : taskBObjects;
        foreach(GameObject x in taskObjects) {              // disable all items & reset all the positions
            x.SetActive(false);
            x.GetComponent<HotkeyPosition>().resetPosition();
        }
        taskObjects[0].SetActive(true);                      // re-enable the first item 
        if(startTaskA) {taskAcurrentActiveIndex = 0;} else {taskBcurrentActiveIndex = 0;}
        taskAObject.SetActive(startTaskA);
        taskBObject.SetActive(!startTaskA);
        playGroundObject.SetActive(false);
        configureGripVariant(taskObjects, useBinaryGrip);
        tellCurrentCondition();
    }

    public void configureGripVariant(List<GameObject> objectList, bool useBinaryGrip) {
        foreach(GameObject g in objectList) {
            VariableGrip vg = g.GetComponentInChildren<VariableGrip>();
            vg.gripVariant = GripVariant.Binary;
            vg.binaryGripThreshold = useBinaryGrip ? binaryGripThreshold : 0f;
            vg.enableHandUI = useBinaryGrip;
        }
    }

    // takes a list of GameObject and returns a list of all children of those GameObjects
    private List<GameObject> flattenGameObjectList(List<GameObject> parentGameObjects) {
        List<GameObject> children = new List<GameObject>();
        foreach(GameObject g in parentGameObjects) {
            foreach(Transform t in g.transform) {
                children.Add(t.gameObject);
            }
        }
        return children;
    }

    public void enableNextObject() { //unelegant?
        if(currentlyInTaskA) {
            enableNextTaskObject(ref taskAcurrentActiveIndex, taskAObjects);
        } else {
            enableNextTaskObject(ref taskBcurrentActiveIndex, taskBObjects);
        }
    }
    
    private void enableNextTaskObject(ref int currentActiveIndex, List<GameObject> taskObjects) {
        currentActiveIndex++;
        if(currentActiveIndex < taskObjects.Count) {
            taskObjects[currentActiveIndex].SetActive(true);
        } else { //all objects placed into respective goalzones
            loggingManager.writeTaskFinishedToLog(currentlyInTaskA, currentlyUsingBinaryGrip);
            if(taskAThenB && !currentlyInTaskA || !taskAThenB && currentlyInTaskA) { // both tasks finished
                goToNextQuestionnaire();
            } else { //continue with the second task in the condition
                startTask(currentlyUsingBinaryGrip, !taskAThenB);
            }
        }
    }

    private void goToNextQuestionnaire() {
        currentQuestionnaire++;
        questionnaires[currentQuestionnaire].SetActive(true);
        if(       currentQuestionnaire == 1) {
            firstHint.SetActive(true);
        } else if(currentQuestionnaire == 4) {
            secondHint.SetActive(true);
        }
    }

    private void finishedAQuestionnaire() {
        questionnaires[currentQuestionnaire].SetActive(false);
        switch (currentQuestionnaire)
        {
            case 0:
                activateDemographics = false;
                configureTaskObjectActivity();
                break;
            case 3:
                //start task with next condition
                currentlyUsingBinaryGrip = !currentlyUsingBinaryGrip;
                currentlyInTaskA = taskAThenB;
                startTask(currentlyUsingBinaryGrip, taskAThenB);
                break;
            default:

                switch (currentQuestionnaire)
                {
                    case 1:
                        firstHint.SetActive(false);
                        break;
                    case 4: 
                        secondHint.SetActive(false);
                        break;
                }

                if (currentQuestionnaire != 6)
                { // continue if the finished questionnaire wasn't the last one (index 6) and no next task has been started
                    goToNextQuestionnaire();
                }

                break;
        }

    }
    
    // tells the vrQuestionnaire framework what conditions and which participant are currently active so filenames are correct.
    private void tellCurrentCondition() {
        string condition = "";
        condition += "A_Then_B_" + taskAThenB.ToString();
        condition += "_bin_Then_fix_" + binaryThenFixed.ToString();
        condition += "_ThisCond_";
        condition += currentlyUsingBinaryGrip ? "Bin" : "Fix";
        studySetup.Condition = condition;
        studySetup.ParticipantId = participantID;
    }

    public void moveAllItemsToGoal() {
        foreach(GameObject o in taskAObjects) {
            resetAndMoveItemToGoal(o, soupGoalA);
        }
        GameObject goal;
        foreach(GameObject o in taskBObjects) {
            if(o.name.Contains("Soup")) {
                goal = soupGoalB;
            } else if(o.name.Contains("Milk")) {
                goal = milkGoal;
            } else {
                goal = bookGoal;
            }
            resetAndMoveItemToGoal(o, goal);
        }
    }

    private void resetAndMoveItemToGoal(GameObject item, GameObject goal) {
            item.GetComponent<HotkeyPosition>().resetPosition();
            item.transform.position = goal.transform.position;
            item.transform.rotation = goal.transform.rotation;
    }

}
