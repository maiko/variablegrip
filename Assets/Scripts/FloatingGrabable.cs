using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SG;
public class FloatingGrabable : SG_Grabable
{
    //maiko: doesnt update the rotation, which just results in the hand not rotating either, so it stays unused in the project
    protected override void UpdateLocation(float dT)
            {
                List<GrabArguments> heldBy = this.grabbedBy;
                if (heldBy.Count > 0) //I'm actually grabbed by something
                {
                    Vector3 targetPosition; Quaternion targetRotation;
                    CalculateTargetLocation(heldBy, out targetPosition, out targetRotation);
                    MoveToTargetLocation(targetPosition, this.transform.rotation, dT);
                }
            }

}
