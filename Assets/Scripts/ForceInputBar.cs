using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ForceInputBar : MonoBehaviour
{
    [Range(0, 100)]
    public float startValue;

    [Range(0, 100)]
    public float endValue;

    private float rotation;

    private float endFillValue;

    private Image image;

    private Vector3 rotationVector;

    public bool started {get; private set;} = false;

    // Start is called before the first frame update
    void Start()
    {
        rotationVector = Vector3.zero;
        image = GetComponent<Image>();
        image.fillAmount = 0;
        started = true;
    }

    // Update is called once per frame
    void Update()
    {
        rotationVector.z = Mathf.Lerp(0, -180, startValue / 100); //gets a rotation based on the startValue
        this.transform.localRotation = Quaternion.Euler(rotationVector);
        endFillValue = (endValue - startValue) / 200f; // forceInput is between 0 and 100. The fill amount is between 0 and 0.5; this value is visualized by 180 degrees radial filling
    }

    public void fill(float forceValue) {
        if(forceValue <= startValue) {
            image.fillAmount = 0f;
        } else {
            float inBetweenPercent = Mathf.InverseLerp(startValue, endValue, forceValue);
            image.fillAmount = Mathf.Lerp(0, endFillValue, inBetweenPercent);
        }
    }
}
