/*
 * Edited by: Maiko Huebinger, based on the following:
 *
 * LoggingSystem.cs
 *
 * Project: Log2CSV - Simple Logging System for Unity applications
 *
 * Supported Unity version: 5.4.1f1 Personal (tested)
 *
 * Author: Nico Reski
 * Source: https://github.com/nicoversity/unity_log2csv
 * Web: http://reski.nicoversity.com
 * Twitter: @nicoversity
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using SG;
using System.Threading.Tasks;
using System.Linq;


public class LoggingManager : MonoBehaviour {

	#region FIELDS

	// static log file names and formatters
	private static string LOGFILE_DIRECTORY = "log2csv_logfiles";
	private static string LOGFILE_NAME_BASE = "_log_file.csv";
	private static string LOGFILE_NAME_TIME_FORMAT = "yyyy-MM-dd_HH-mm-ss";	// prefix of the logfile, created when application starts (year - month - day - hour - minute - second)

	// logfile reference of the current session
	private string logFile;

	// bool representing whether the logging system should be used or not (set in the Unity Inspector)
	public bool activeLogging;

	public float avgStartForceDuration = 0.3f;

	public bool logAllGrabsAndReleases = false; //Could also create a boolean in VariableGrip.cs that solely enables logging for the individual object 

	public GameObject handUI;

	private HandUIController handUIController;

	private TMPro.TMP_Text tmpText;

	private VariableGrip variableGrip; //reference to the last object with variable grip, provided when the variableGrip calls labelCurrentGrasp
	
    public int addHoursToUTC = 2;

    private string participantID = ""; //do not set here, the value will be set by experiment manager

    private bool setupFinished = false;

    private int currentItemGrasps = 0; // amount of grasps on the current item

    private float currentItemStartTimeStamp = 0f; // the timestamp of the moment the current item became available to place

    private float currentItemGrabbedDuration = 0f; // the amount of time the current item was is held in the hand

    private float currentItemFixedDuration = 0f; // the amount of time the current item is held in a fixed position

    private float firstGraspTimeStamp = 0f; // the timestamp of the moment the current item is grabbed the first time

    private float lastReleaseTimeStamp = 0f; // the timestamp of the moment the current item was released the last time

    private float currentGraspStartTimeStamp = 0f; // the timestamp of the start of the latest grasp attempt

    private float currentFixedPhaseStartTimeStamp = 0f; // the timestamp of the start of the latest phase where the item is grabbed so firm that it is fixed to the hand

    private bool lockNotifyItemNotFixedAnymore = false; // a quick hack: the notifyItemNotFixedAnymore() method always gets called one time the moment an item is grabbed, which is a false alarm. 
    
    private List<float> currentItemHeuristicScores = new List<float>();

    private bool started = false;
    #endregion



	#region START_UPDATE

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start () {
	}

    void Setup() {
        if(this.activeLogging)
		{
             // check if directory exists (and create it if not)
            if(!Directory.Exists(LOGFILE_DIRECTORY)) Directory.CreateDirectory(LOGFILE_DIRECTORY);

            // create file for this session using time prefix based on standard UTC time
            this.logFile = LOGFILE_DIRECTORY
                + "/"
                + "participant-"+ participantID + "-"
                //+ System.DateTime.UtcNow.ToString(LOGFILE_NAME_TIME_FORMAT)
                + System.DateTime.UtcNow.AddHours((double)addHoursToUTC).ToString(LOGFILE_NAME_TIME_FORMAT)	// manually adjust time zone, e.g. + 2 UTC hours for summer time in location Stockholm/Sweden
                + LOGFILE_NAME_BASE;
            File.Create(this.logFile);

            if(File.Exists(this.logFile)) Debug.Log("[LoggingSystem] LogFile created at " + this.logFile);
            else Debug.LogError("[LoggingSystem] Error creating LogFile");

            tmpText = handUI.GetComponentInChildren<TMPro.TMP_Text>(); //used for debugging purposes (handUI can show heuristic output)
            handUIController = handUI.GetComponent<HandUIController>();
		}
        started = true;


    }

    public void setParticipantID(string _participantID) { //makes sure that the participantID gets transferred early enough before the log file gets created
        this.participantID = _participantID;
        if(!setupFinished) Setup(); //setup should only run once
        setupFinished = true; //in case the method somehow gets called twice this makes sure that setup() only runs once
    }

	#endregion



	#region WRITE_TO_LOG

	/// <summary>
	/// Writes the message to the log file on disk.
	/// </summary>
	/// <param name="message">string representing the message to be written.</param>
	public void writeMessageToLog(string message)
	{
		if(this.activeLogging)
		{
			if(File.Exists(this.logFile))
			{
				TextWriter tw = new StreamWriter(this.logFile, true);
				tw.WriteLine(message);
				tw.Close(); 
			}
		}
	}

	/// <summary>
	/// Writes the message including timestamp to the log file on disk.
	/// </summary>
	/// <param name="message">string representing the message to be written.</param>
	public void writeMessageWithTimestampToLog(string message)
	{
		writeMessageToLog(Time.realtimeSinceStartup.ToString() + ";" + message);
	}


	/// <summary>
	/// Writes an Action-Object-Target message including timestamp to the log file on disk.
	/// </summary>
	/// <param name="act">string representing the ACTION message.</param>
	/// <param name="obj">string representing the OBJECT message.</param>
	/// <param name="tar">string representing the TARGET message.</param>
	public void writeAOTMessageWithTimestampToLog(string act, string obj, string tar)
	{
		writeMessageToLog(Time.realtimeSinceStartup.ToString() + ";" + act + ";" + obj + ";" + tar);
	}
		

	/// <summary>
	/// Writes the Action-Object-Target-Origin-State message with timestamp to log.
	/// </summary>
	/// <param name="act">string representing the ACTION message.</param>
	/// <param name="obj">string representing the OBJECT message.</param>
	/// <param name="tar">string representing the TARGET message.</param>
	/// <param name="origin">string representing the ORIGIN message.</param>
	/// <param name="state">string representing the STATE message.</param>
	/// <param name="state">string representing the MODE message.</param>
	public void writeAOTOSMMessageWithTimestampToLog(string act, string obj, string tar, string origin, string state, string mode)
	{
		writeMessageToLog(Time.realtimeSinceStartup.ToString() + ";" + act + ";" + obj + ";" + tar + ";" + origin + ";" + state + ";" + mode);
	}

	#endregion
    // format: Timestamp;Successfull Placement (event identifier) Item name; Distance Offset; Offset Angle; Total Duration until placement; Amount of Grabs; Duration grabbed; Duration Fixed; Duration loose; Duration ungrabbed in between; Duration total ungrabbed; Avg. Heuristic Score

    // writes metrics from an object placed onto a goalzone as in GoalZone.cs to the log
    public void writePlacementToLog(GameObject placedObject, GameObject goal, float angleOffset, Vector3 offsets) {
        float now = Time.realtimeSinceStartup - 0.5f; //subtract the 0.5 seconds added as delay before vanishing the placed object
        float totalDurationOfCurrentItem = now - currentItemStartTimeStamp;
        float ungrabbedDuration = totalDurationOfCurrentItem - currentItemGrabbedDuration;
        float inactiveDuration = (firstGraspTimeStamp - currentItemStartTimeStamp) + (now - lastReleaseTimeStamp); // the duration of time in which the participant has not started + has finished the placement 
        float looseDuration = currentItemGrabbedDuration - currentItemFixedDuration;
        float recoveryDuration = ungrabbedDuration - inactiveDuration;
        float averageHeuristicScore = currentItemHeuristicScores.Count != 0 ? currentItemHeuristicScores.Average() : -1;
        string message = " Successful Placement; "
        + placedObject.transform.parent.gameObject.name  + "; "         // name, derived from parent eg. Cube (5)
        //+ offsets.x + "; " + offsets.y + "; " + offsets.z + "; "      // offset x; offset y; offset z; not really interesting, if given the distance
        + offsets.magnitude + "; "                                      // distance;
        + angleOffset + "; "                                            
        + totalDurationOfCurrentItem + "; "                             // total duration of current item from start to placement
        + currentItemGrasps + "; "                                      // amount of grasps for this item
        + currentItemGrabbedDuration + "; "                             // duration the current item was in a grabbed state
        + currentItemFixedDuration + "; "                               // duration the current item was held with a fixed grip
        + looseDuration + "; "                                          // duration the current item was held with a loose grip
        + recoveryDuration + "; "                                       // the duration of time spend ungrabbed after the first grab and before the last grab 
        + ungrabbedDuration + "; "                                      // duration the current item was not grabbed
        + averageHeuristicScore + ";";                                  // average of the heuristic scores for all grabes of the current item                      
        writeMessageWithTimestampToLog(message);
        resetFields();
    }

    private void resetFields() {
        currentItemStartTimeStamp = Time.realtimeSinceStartup;
        currentItemFixedDuration = 0f;
        currentItemGrabbedDuration = 0f;
        currentItemGrasps = 0;
        firstGraspTimeStamp = 0f;
        lastReleaseTimeStamp = 0f;
        currentItemHeuristicScores.Clear();
    }

    //format: timestamp; Grabbed (event identifier); grabbed item name; distance to goalZone; goalZone name;
    public void writeGrabbedToLog(SG_Interactable interactable) {
        float currentTime = Time.realtimeSinceStartup;
        lockNotifyItemNotFixedAnymore = true;
        currentGraspStartTimeStamp = currentTime;
        if(firstGraspTimeStamp > 0f) firstGraspTimeStamp = currentTime;
        writeMessageToLog(currentTime.ToString() + "; Grabbed; " + getDistanceToGoalZoneAsString(interactable)); //takes the parent's name
        currentItemGrasps++;
    }

    //format: timestamp: Released (event identifier); released item name; distance to goalZone; goalzone name; duration of grasp;
    public void writeReleasedToLog(SG_Interactable interactable) {
        float currentTime =  Time.realtimeSinceStartup;
        lastReleaseTimeStamp = currentTime;
        float currentGraspDuration = currentTime - currentGraspStartTimeStamp; // the time this grasp took in total
        currentItemGrabbedDuration += currentGraspDuration;                        // add it to the sum
		string itemAndGoalZoneInfo = getDistanceToGoalZoneAsString(interactable);
		string message = currentTime + "; " +
                        "Released; " +
                        itemAndGoalZoneInfo + " " +
                        currentGraspDuration + ";";     // duration of the current grasp
        writeMessageToLog(message); 
    }

    //format: grabbed/released item name; distance to goalZone; goalZone name;
    // if no goalZone is found, it just returns the name of the item
    private string getDistanceToGoalZoneAsString(SG_Interactable interactable) {
        Transform parentTrans = interactable.transform.parent;
        RenderedObjectHelper renderedObjectHelper = parentTrans.GetComponentInChildren<RenderedObjectHelper>();
        Transform renderedObjectTrans = renderedObjectHelper.transform;
		GameObject goalZoneGO = renderedObjectHelper.goalZone;
        string message = parentTrans.gameObject.name + "; "; //the name of the item
        if(goalZoneGO != null) {
            float distanceToGoal = (renderedObjectTrans.position - goalZoneGO.transform.position).magnitude;
            message +=  distanceToGoal + "; " +         // distance to the goalzone of the item
                        goalZoneGO.name + ";";         // name of the goalZone
        }
        return message;
    }

    //format: Timestamp; Task changed (event identifier); Task Number; Mode identifier;
    public async Task writeCurrentTaskConfigurationToLog(bool currentlyInTaskA, bool currentlyUsingBinaryGrip) {
        float now = Time.realtimeSinceStartup; //save before waiting
        while(!started) await Task.Yield(); //needs to make sure the file is ready to be written to because this method gets called quite early
        writeMessageToLog(now + "; Task changed; " + getTaskAndModeCombo(currentlyInTaskA, currentlyUsingBinaryGrip));
    }

    //format: Timestamp; Task finished (event identifier); Task Number; Mode identifier;
    public void writeTaskFinishedToLog(bool currentlyInTaskA, bool currentlyUsingBinaryGrip) {
        writeMessageWithTimestampToLog("Task finished" + getTaskAndModeCombo(currentlyInTaskA, currentlyUsingBinaryGrip));
    }

    private string getTaskAndModeCombo(bool currentlyInTaskA, bool currentlyUsingBinaryGrip) {
        string task = currentlyInTaskA ? "A ;" : "B ;";
        string mode = currentlyUsingBinaryGrip ? "Binary;" : "Fixed;";
        return task + mode;
    }

    // A heuristic for determining if the current grasp hit the grabbed object in a good way, meaning that the force can be robustly controlled by the wearer of the glove
    public async Task labelCurrentGrasp(VariableGrip variableGrip) {
		this.variableGrip = variableGrip;
        float score = 0;
        float startTime = Time.time;
        var startForce = averageStartForce();
        var fullForceDuration =  fullForceReachedDuration();
        var weakFingersWereAttached = weakFingersAttachedDuration();
        var maxForceLevel = maxForceLevelReached();
        await Task.WhenAll(startForce, fullForceDuration, weakFingersWereAttached, maxForceLevel);
        float totalDuration = Time.time - startTime;
        
        string debugText = "";
        // didn't start with an initially low force
        if(startForce.Result > 30f) { 
            score += 1;
            debugText += "didn't start with an initially low force (" + startForce.Result + ")\n";
        }

        // reached full force
        if(fullForceDuration.Result > 0f) { 
            score += 1f;
            debugText += "could reach full force\n";
        }
        // reached full force for more than 300ms
        if(fullForceDuration.Result > 0.3f) { 
            score += 1.5f;
            debugText += "could reach full force for more than 300ms (exactly " + (fullForceDuration.Result / 100) + "ms)\n";
        }

        // more than 10% of the time full force was reached
        if(fullForceDuration.Result / totalDuration > 0.1f) {
            score += 1f;
            debugText += "more than 10% of the time full force was reached (" + (fullForceDuration.Result / totalDuration) * 100 + "%)\n";
        }

        //weaker/secondary fingers were attached for more than 60% of the time
        if(weakFingersWereAttached.Result / totalDuration > 0.6f) {
            score += 0.5f;
            debugText += "weaker/secondary fingers were attached for more than 60% of the time (" + (weakFingersWereAttached.Result / totalDuration) * 100 + "%)\n";
        }

        if(handUIController.showGraspQualityDebugText) {
            tmpText.text = debugText + "\n score " + score;
        }

        currentItemHeuristicScores.Add(score);
		// ; event; score; total duration of grasp; \n debug text
        writeMessageToLog("; grasp quality score; " + score + "; " + totalDuration + "; \n\\debugText{" + debugText + "}"); // \debugtext{...} latex style
    }

    public void notifyItemFixed() {
        currentFixedPhaseStartTimeStamp = Time.realtimeSinceStartup;
    }

    public void notifyItemNotFixedAnymore() {
        if(!lockNotifyItemNotFixedAnymore) {
            currentItemFixedDuration += Time.realtimeSinceStartup - currentFixedPhaseStartTimeStamp;
        } else {
            lockNotifyItemNotFixedAnymore = false; //unlock method for the next (real) call 
        }
    }

    async Task<float> averageStartForce() {
        float iterations = 0f;
        float forceLevelSum = 0f;
        float endTime = Time.time + avgStartForceDuration;
        while (Time.time < endTime) {
            iterations++;
            forceLevelSum += variableGrip.forceLevel;

            await Task.Yield();
        }
        float result = forceLevelSum / iterations;
        
		// writes timestamp; event name; the average force given in message; the measuring duration in seconds;
		writeMessageWithTimestampToLog(" Average force at grab start; " + result + "; " + avgStartForceDuration + "; ");
        return result;
    }

    // measures the cumulative time the force is reaching maximal levels while an item is being held
    async Task<float> fullForceReachedDuration() {
        float duration = 0f;
		float fullForce = 98;
        while(variableGrip.isGrabbed) {
            if(variableGrip.forceLevel >= fullForce) {
                duration += Time.deltaTime;
            }
            await Task.Yield();
        }
		// ; event name; duration that full force was reached; defintion for full force; 
		writeMessageToLog("; Full force reached duration; " + duration + "; " +  fullForce + "; ");
        return duration;
    }

    // measures the time that either the pinky or the ring finger was attached
    async Task<float> weakFingersAttachedDuration() {
        float duration = 0;
        SG_FingerFeedback pinky = variableGrip.allFingers.Find(f => f.finger == SGCore.Finger.Pinky); //should maybe include middle finger as well
        SG_FingerFeedback ring = variableGrip.allFingers.Find(f => f.finger == SGCore.Finger.Ring);
        while(variableGrip.isGrabbed) {
            if(pinky.IsTouching() || ring.IsTouching()) {
                duration += Time.deltaTime;
            }
            await Task.Yield();
        }
		// ; event name;   
		writeMessageToLog("; either pinky or ring attached duration" + duration + "; ");
        return duration;
    }

    // returns the highest forceLevel that was recorded while an item was held
    async Task<float> maxForceLevelReached() {
        float maxForceLevel = 0;
        while(variableGrip.isGrabbed) {
            if(variableGrip.forceLevel > maxForceLevel) {
                maxForceLevel = variableGrip.forceLevel;
            }
            await Task.Yield();
        }
		// ; event name; maximal force level; 
		writeMessageToLog("; maximal force reached during grab; " + maxForceLevel + "; ");
        return maxForceLevel;
    }

}