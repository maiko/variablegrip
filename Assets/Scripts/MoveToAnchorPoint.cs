using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class MoveToAnchorPoint : MonoBehaviour
{    
    private ConfigurableJoint joint;
    
    // Start is called before the first frame update
    void Start()
    {
        joint = GetComponentInParent<ConfigurableJoint>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = joint.anchor;
    }
}
