using System;
using UnityEngine;
using SG;
using System.Collections;
using System.Collections.Generic;

// Author: Maiko H.

public enum Equation {
    Linear,
    Squared,
    Cubic,
    Hypercubic
};

public enum GripVariant {
    CustomStatic,
    Binary,
    Variable
}

public enum ForceInputVariant {
    All,
    OnlyTouchingFingers,

    ThumbAndIndex,

    ThumbAndIndexAndMiddle,

    Index
}

public enum AnchorAxis {
    X,
    Y,
    Z
}

public class VariableGrip : MonoBehaviour
{

    public AnimationCurve unusedAnimationCurveWithNoEffect; //Use this instead of equation

    private VariableGripSetup variableGripSetup;

    private HandUIController handUIController;

    public float multiplicator = 0.01f;

    public Equation equation = Equation.Linear;

    [Range(0,99)]
    public float lowForceThreshold = 0f; // the force level that must be exceeded to change achieve a variable Grip, anything lower will result in the same output value which is defined bz positionalSpringFloor 

    public float positionalSpringFloor = 0f; // the lowest spring value that is at least applied when grabbed

    public ForceInputVariant forceInputVariant = ForceInputVariant.OnlyTouchingFingers;

    public GripVariant gripVariant = GripVariant.Variable;

    public List<SG_FingerFeedback> allFingers {get; private set;}

    private SG_HandFeedback handFeedbackComponent;

    private ConfigurableJoint cj;

    private FixedJoint fj;

    private JointDrive slerp;

    private Rigidbody rb;

    private Rigidbody joinedObject;

    private Collider[] joinedColliders;

    public float forceLevel;

    [SerializeField]
    private float finalSpring;

    public float targetSpring;

    private Transform thumbTip;

    private Transform indexTip;

    private GameObject fingerMiddle;

    public bool fixedJointIsAttached = false;

    public bool lowerThanLowForceThreshold {get; private set;} = false;

    public bool isGrabbed {get; private set;} = false;

    [Range(0,100)]
    public float fixedJointThreshold = 95;

    [Range(0,100)]
    public float binaryGripThreshold = 50;

    private List<SG_FingerFeedback> chosenFingers;

    private MeshRenderer meshRenderer;

    public GameObject childWithMesh; //optional, in case a child object has the meshrenderer component

    private LoggingManager loggingManager;

    public bool stronglyAvoidReleaseCollisionsWithHand; //enables a stronger avoidance of the collision between hand and grabbed object that also includes the renderedObject

    private List<float> lastForceValues;

    public int movingAverageWindowsSize = 10;

    public bool useMovingAverage = false;

    public bool useAnchorAxis = false; // if enabled, forces the anchor to move along the y axis. Make sure to place the Anchor in the center. Don't enable after grabbing as the first grab will move the anchor away off path

    public AnchorAxis anchorAxis = AnchorAxis.Y;

    private MeshRenderer anchorMeshRenderer;

    public AnimationCurve alternativeForceResponse = AnimationCurve.Linear(0, 0, 1, 1);

    public bool useAlternativeForceResponse = false;
    // Start is called before the first frame update

    public bool enableMeshRendererDuringGrab = false;

    public bool enableHandUI = true;
    void Start()
    {
        GameObject setupObject = GameObject.Find("Variable Grip Setup");
        variableGripSetup = setupObject.GetComponent<VariableGripSetup>();
        if(variableGripSetup == null) {
            throw new Exception("You need a game object in the scene called \"Variable Grip Setup\" with the script of the same name attached to it and configured.");
        }
        StartCoroutine(WaitForSetup());

        GameObject experimentManager = GameObject.Find("Experiment Manager");
        if(experimentManager == null) {
            throw new Exception("You need a game object in the scene called \"Experiment Manager\" with the script of the same name attached to it and configured.");
        }
        loggingManager = experimentManager.GetComponent<LoggingManager>();

        indexTip = variableGripSetup.indexTip;
        thumbTip = variableGripSetup.thumbTip;
        fingerMiddle = variableGripSetup.FingerMiddle;

        handFeedbackComponent = variableGripSetup.feedbackLayer.GetComponent<SG_HandFeedback>();
        allFingers = new List<SG_FingerFeedback>();
        allFingers.AddRange(handFeedbackComponent.fingerFeedbackScripts);

        rb = this.gameObject.GetComponent<Rigidbody>();

        //create configurable joint
        cj = gameObject.GetComponent<ConfigurableJoint>();
        slerp = cj.slerpDrive;
        joinedObject = cj.connectedBody;

        List<Collider> colliders = new List<Collider>();
        colliders.AddRange(joinedObject.gameObject.GetComponentsInChildren<Collider>());
        colliders.AddRange(joinedObject.gameObject.GetComponents<Collider>());
        joinedColliders = colliders.ToArray();

        chosenFingers = new List<SG_FingerFeedback>();

        attachFixedJoint();
        
        meshRenderer = GetComponent<MeshRenderer>();
        if(meshRenderer == null) {
            meshRenderer = childWithMesh.GetComponent<MeshRenderer>();
        }

        meshRenderer.enabled = false;

        //TODO find the thumbTip and IndexTip ob your own, currently dragged into the component
        // SG_FingerFeedback thumb = Array.Find<SG_FingerFeedback>(allFingers, e => e.finger == SGCore.Finger.Thumb);
        // SG_FingerFeedback index = Array.Find<SG_FingerFeedback>(allFingers, e => e.finger == SGCore.Finger.Index);
        // thumbTip = thumb.gameObject.transform.GetChild(0).GetChild(0);
        // indexTip = index.gameObject.transform.GetChild(0).GetChild(0);

        lastForceValues = new List<float>();

        //TODO: doesn't work as expected so the listeners still need to be assigned in the editor.
        SG_Grabable sg_grabable = GetComponent<SG_Grabable>();
        sg_grabable.ObjectGrabbed.AddListener(onGrab);
        sg_grabable.ObjectReleased.AddListener(onRelease);

        anchorMeshRenderer = GetComponentInChildren<MoveToAnchorPoint>().gameObject.GetComponent<MeshRenderer>();
        anchorMeshRenderer.enabled = false;
    }

    IEnumerator WaitForSetup() {
        yield return new WaitUntil(() => variableGripSetup.started);
        handUIController = variableGripSetup.handUIController;
    }
    
    void Update() {
        if(Input.GetKeyDown(KeyCode.P)) {
            StartCoroutine(moveHandleToRenderedObject());
        }

    }

    void FixedUpdate()
    {
        if (!isGrabbed) return;

        // Gets a force reading from left hand
        forceLevel = calculateForce();
        if (useMovingAverage) forceLevel = movingAverage(forceLevel);

        manageFixedJoint();

        if (gripVariant == GripVariant.Variable)
        {

            if (equation == Equation.Linear)
            {
                finalSpring = forceLevel * multiplicator;
            }
            else if (equation == Equation.Squared)
            {
                finalSpring = multiplicator * Mathf.Pow(forceLevel, 2);
            }
            else if (equation == Equation.Cubic)
            {
                finalSpring = multiplicator * Mathf.Pow(forceLevel, 3);
            }
            else
            { //Hypercubic
                finalSpring = multiplicator * Mathf.Pow(forceLevel, 4);
            }

            if (lowForceThreshold > 0f)
            { //floor could be integrated into binary grip as well
                if (forceLevel < lowForceThreshold)
                {
                    finalSpring = positionalSpringFloor;
                    lowerThanLowForceThreshold = true;
                }
                else
                {
                    lowerThanLowForceThreshold = false;
                }
            }

        }
        else if (gripVariant == GripVariant.Binary)
        { // just makes sure the values are correct, not very elegant, would need something like onChange()
            finalSpring = positionalSpringFloor;
            fixedJointThreshold = binaryGripThreshold;
        }
        else
        {
            finalSpring = targetSpring;
        }


        // calculates the difference in rotation between the grabbable handle and the rendered object. Spring will jump by this rotation.
        cj.targetRotation = Quaternion.Inverse(transform.rotation) * joinedObject.transform.rotation;
        slerp.positionSpring = finalSpring;
        cj.slerpDrive = slerp;
    }

    // calculates a mean force depending on the chosen variant: all Fingers, only touching or just index with thumb
    float calculateForce() {
        
        float forceSum = 0;
        int touchingFingers = 0;

        if(forceInputVariant == ForceInputVariant.OnlyTouchingFingers) {
            chosenFingers.AddRange(allFingers.FindAll(f => f.IsTouching()));
        } else if(forceInputVariant == ForceInputVariant.Index) {
            chosenFingers.AddRange((allFingers.FindAll(f => f.finger == SGCore.Finger.Index)));
        } else if(forceInputVariant == ForceInputVariant.ThumbAndIndex) {
            chosenFingers.AddRange((allFingers.FindAll(f => f.finger == SGCore.Finger.Thumb || //OR
                                                            f.finger == SGCore.Finger.Index)));
        } else if(forceInputVariant == ForceInputVariant.ThumbAndIndexAndMiddle) {
            chosenFingers.AddRange((allFingers.FindAll(f => f.finger == SGCore.Finger.Thumb || //OR
                                                            f.finger == SGCore.Finger.Index || //OR
                                                            f.finger == SGCore.Finger.Middle)));
        } else {
            chosenFingers = allFingers;
        }

        foreach(SG_FingerFeedback finger in chosenFingers) {
            forceSum += useAlternativeForceResponse ? getAlternativeForceLevel(finger) : finger.ForceLevel; //TODO consider moving this code to an extension of SG_material
            touchingFingers++;
        }
        // foreach(SG_FingerFeedback finger in chosenFingers) {
        //     forceSum += finger.ForceLevel;
        //     touchingFingers++;
        // }

        chosenFingers.Clear();
        return forceSum / touchingFingers;
    
    }

    private int getAlternativeForceLevel(SG_FingerFeedback finger) {
        if(finger.IsTouching()) {
            return SG_Material.CalculateResponseForce(finger.DistanceInCollider, finger.TouchedMaterialScript.maxForce, finger.TouchedMaterialScript.maxForceDist, ref alternativeForceResponse);
        } else {
            return 0;
        }
    }

    void manageFixedJoint() {
        
        if(isGrabbed) {

            if(fixedJointIsAttached) { //check if fj should be destroyed

                if(forceLevel < fixedJointThreshold - 4 || gripVariant == GripVariant.CustomStatic) { // -4 because otherwise the joint will repeatedly be created and destroyed causing lag
                    destroyFixedJoint();
                    loggingManager.notifyItemNotFixedAnymore();
                }

            } else { //check if fj should be created

                if(forceLevel >= fixedJointThreshold && gripVariant != GripVariant.CustomStatic) {
                    attachFixedJoint();
                    loggingManager.notifyItemFixed();
                }

            }

        }

    }

    void attachFixedJoint() {

        if(!fixedJointIsAttached) {

            fj = this.gameObject.AddComponent<FixedJoint>();
            fj.connectedBody = joinedObject;
            fixedJointIsAttached = true;

        }

    }

    void destroyFixedJoint() {
        Destroy(fj);
        fixedJointIsAttached = false;
    }

    public void onGrab(SG_Interactable interactable, SG_GrabScript grabScript) {
        if(loggingManager.logAllGrabsAndReleases) {
            loggingManager.writeGrabbedToLog(interactable);
        }
        isGrabbed = true;
        var task = loggingManager.labelCurrentGrasp(this);
        updateAnchorPosition();
        handUIController.currentlyHeldObjectVG = this;
        if(enableHandUI) handUIController.enabled = true;

        if(enableMeshRendererDuringGrab) meshRenderer.enabled = true;
        gameObject.layer = 6; // set to grabables layer which cannot collide with default the layer
        variableGripSetup.makePenetrable();
        anchorMeshRenderer.enabled = true;
    }

    public void onRelease(SG_Interactable interactable, SG_GrabScript grabScript) { 
        if(loggingManager.logAllGrabsAndReleases) {
            loggingManager.writeReleasedToLog(interactable);
        }

        StartCoroutine(moveHandleToRenderedObject()); //does it even help to use a coroutine?

        isGrabbed = false;
        handUIController.enabled = false;
        meshRenderer.enabled = false;

        if(stronglyAvoidReleaseCollisionsWithHand) {
            SG_HandPhysics handphysics = grabScript.HandPhysics;
            handphysics.SetIgnoreCollision(joinedColliders, true);
            handphysics.MarkForUncollision(rb, joinedColliders);
            //handphysics.returnCollliders(); ?
        }
        gameObject.layer = 0; // reset to default layer
        variableGripSetup.makeInpenetrable();
        anchorMeshRenderer.enabled = false;
    }

    private void updateAnchorPosition() {
        Vector3 fMPos = this.transform.InverseTransformPoint(fingerMiddle.transform.position);
        if(useAnchorAxis) {
            Vector3 newPos = cj.anchor;
            if(anchorAxis == AnchorAxis.X) {
                newPos.x = fMPos.x;
            } else if (anchorAxis == AnchorAxis.Y) {
                newPos.y = fMPos.y;
            } else {
                newPos.z = fMPos.z;
            }
            cj.anchor = newPos;
        } else {
            cj.anchor = fMPos;
        }
    }

    IEnumerator moveHandleToRenderedObject() {
        int i = 0;
        while(i < 3) {
            slerp.positionSpring = 0f;
            cj.slerpDrive = slerp;

            if(fixedJointIsAttached) {
                destroyFixedJoint();
            }
            
            yield return new WaitForEndOfFrame();
            
            this.transform.position = joinedObject.transform.position;
            this.transform.rotation = joinedObject.transform.rotation;
            attachFixedJoint(); 
            i++;
        }
        
    }

    private float movingAverage(float newestValue) {
        if(lastForceValues.Count == movingAverageWindowsSize) {
            lastForceValues.RemoveAt(lastForceValues.Count -1);
        }
        if(lastForceValues.Count > movingAverageWindowsSize) { //useful for testing if the windows size gets smaller while the list is alredy populated
            lastForceValues = lastForceValues.GetRange(0, movingAverageWindowsSize -1);
        }
        lastForceValues.Insert(0, newestValue);
        float sum = 0f;
        lastForceValues.ForEach(x => sum += x);
        return sum / lastForceValues.Count;
    }
    
}

